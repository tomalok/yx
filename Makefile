# vim: noet ts=8:

CC?=cc
PREFIX?=/usr

.PHONY: test clean

yx: yx.c
	$(CC) -Os -o yx yx.c -lyaml
	chmod +x yx

test: yx
	(cd tests && ./do_tests.sh)

install: yx
	install -sDm0755 -t "$(PREFIX)"/bin yx
	install -Dm0644 -t "$(PREFIX)"/share/man/man1 yx.1

clean:
	rm yx
